package misyac.yuri.homework1.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import misyac.yuri.homework1.R;

public class AActivity extends BaseActivity {

    private static final String ALIAS = "A";

    public static void start(Context context, String info, boolean b) {
        Intent intent = new Intent(context, AActivity.class);
        intent.putExtra(EXTRA_INFO, info);
        if (b) {
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mInfo != null) {
            mInfo = mInfo + SEPARATOR + ALIAS;
        } else {
            mInfo = ALIAS;
        }
        mStackInfo.setText(mInfo);

        final boolean b = getSharedPreferences(getString(R.string.pref), MODE_PRIVATE)
                .getBoolean(getString(R.string.is32task), false);
        mGoToNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BActivity.start(AActivity.this, mInfo, b);
            }
        });

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


    }
}
