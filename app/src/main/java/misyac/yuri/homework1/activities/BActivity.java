package misyac.yuri.homework1.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class BActivity extends BaseActivity {

    private static final String ALIAS = "B";

    public static void start(Context context, String info, boolean b) {
        Intent intent = new Intent(context, BActivity.class);
        intent.putExtra(EXTRA_INFO, info);
        if (b) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mInfo = mInfo + SEPARATOR + ALIAS;
        mStackInfo.setText(mInfo);

        mGoToNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CActivity.start(BActivity.this, mInfo);
            }
        });

    }

}
