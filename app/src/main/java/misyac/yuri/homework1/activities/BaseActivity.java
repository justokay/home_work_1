package misyac.yuri.homework1.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import misyac.yuri.homework1.R;

public abstract class BaseActivity extends AppCompatActivity {

    protected static final String EXTRA_INFO = BaseActivity.class.getCanonicalName() + "#extra_info";
    protected static final String SEPARATOR = "->";

    private static final String STATE_INFO = BaseActivity.class.getCanonicalName() + "#state_info";

    protected Button mGoToNextActivity;
    protected TextView mStackInfo;
    protected String mInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        mGoToNextActivity = ((Button) findViewById(R.id.go_to_next));
        mStackInfo = (TextView) findViewById(R.id.stack_info);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        String info = getIntent().getStringExtra(EXTRA_INFO);
        if (info != null) {
            mInfo = info;
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putString(STATE_INFO, mInfo);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
