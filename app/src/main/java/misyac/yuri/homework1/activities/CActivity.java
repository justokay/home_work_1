package misyac.yuri.homework1.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CActivity extends BaseActivity {

    private static final String ALIAS = "C";

    public static void start(Context context, String info) {
        Intent intent = new Intent(context, CActivity.class);
        intent.putExtra(EXTRA_INFO, info);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mInfo = mInfo + SEPARATOR + ALIAS;
        mStackInfo.setText(mInfo);

        mGoToNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DActivity.start(CActivity.this, mInfo);
            }
        });

    }

}
