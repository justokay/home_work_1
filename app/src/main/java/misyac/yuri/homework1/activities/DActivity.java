package misyac.yuri.homework1.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import misyac.yuri.homework1.R;

public class DActivity extends BaseActivity {

    private static final String ALIAS = "D";
    private boolean is32task;

    public static void start(Context context, String info) {
        Intent intent = new Intent(context, DActivity.class);
        intent.putExtra(EXTRA_INFO, info);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mInfo = mInfo + SEPARATOR + ALIAS;
        mStackInfo.setText(mInfo);

        is32task = getSharedPreferences(getString(R.string.pref), MODE_PRIVATE)
                .getBoolean(getString(R.string.is32task), false);
        mGoToNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AActivity.start(DActivity.this, mInfo, is32task);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!is32task) {
            goToB();
        } else {
            super.onBackPressed();
        }
    }

    private void goToB() {
        Intent intent = new Intent(this, BActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!is32task) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    goToB();
                    break;
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
