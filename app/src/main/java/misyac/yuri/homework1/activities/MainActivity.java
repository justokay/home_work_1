package misyac.yuri.homework1.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import misyac.yuri.homework1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Switch switchB = (Switch) findViewById(R.id.switchB);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSharedPreferences(getString(R.string.pref), MODE_PRIVATE)
                        .edit()
                        .putBoolean(getString(R.string.is32task), switchB.isChecked())
                        .commit();

                startActivity(new Intent(MainActivity.this, AActivity.class));
            }
        });

    }
}
